# Cacheable Product Search #

### Cacheable Product Search for the Semantics3 Database. ###

* You can Search for Products in the Semantics3 DataBase.
* All search terms and results not in the cache are cached upon searching.
* Admins Can Configure and Pre-Fetch/Warm a set of Search Terms via the Cache Settings Menu.
* CPS uses MemCache for Caching (could use REDIS,etc...)
* CPS uses PGSQL to store and configure PreFetchable Search Terms
* CPS uses Sidekiq/REDIS for CacheWarming ActionJobs
* MarkUp is a mixture of HAML and ERB
* Site Uses Bootstrap/Material Desgin
* **Note**: This search only returns "live" products

### Setup - on a mac (something akin to; not exact) ###
```
#!bash
git clone git@bitbucket.org:jeanpaulcozzatti/fpp.git
(I assume you have Rails and Brew installed)
cd fpp; 
brew install memcache
brew install redis
bundle
rake db:migrate
memcached -vv
redis-server
bundle exec sidekiq
rails s

heroku local is working

```

### Notes ###

#### to check MemCacheSlabs ####
```
#!bash

rails runner checkMemCache
```
#### to check SideKiq status ####
http://localhost:3000/sidekiq (or your host) 

### BACKLOG - TODO ###
* Google Auth for Admin
* Pagination
* Cache Clearing
* Dev vs Prod (Heroku) Env and Commands
