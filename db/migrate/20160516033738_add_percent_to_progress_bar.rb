class AddPercentToProgressBar < ActiveRecord::Migration
  def change
    add_column :progress_bars, :percent, :float
  end
end
