class CreateProgressBars < ActiveRecord::Migration
  def change
    create_table :progress_bars do |t|
      t.string :message

      t.timestamps null: false
    end
  end
end
