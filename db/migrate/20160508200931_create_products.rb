class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :sem3_id
      t.string :name
      t.text :sitedetails
      t.string :brand
      t.string :cat_id
      t.string :category
      t.string :color
      t.timestamp :created_at
      t.string :description
      t.string :ean
      t.text :features
      t.text :gtins
      t.string :geo
      t.float :height
      t.text :images
      t.integer :images_total
      t.float :length
      t.string :manufacturer
      t.string :model
      t.string :mpn
      t.float :price
      t.string :price_currency
      t.string :size
      t.string :upc
      t.timestamp :updated_at
      t.string :variation_id
      t.text :variation_secondaryids
      t.float :weight
      t.float :width

      t.timestamps null: false
    end
  end
end
