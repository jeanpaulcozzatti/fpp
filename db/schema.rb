# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160516033738) do

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "title"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "sem3_id"
    t.string   "name"
    t.text     "sitedetails"
    t.string   "brand"
    t.string   "cat_id"
    t.string   "category"
    t.string   "color"
    t.datetime "created_at",             null: false
    t.string   "description"
    t.string   "ean"
    t.text     "features"
    t.text     "gtins"
    t.string   "geo"
    t.float    "height"
    t.text     "images"
    t.integer  "images_total"
    t.float    "length"
    t.string   "manufacturer"
    t.string   "model"
    t.string   "mpn"
    t.float    "price"
    t.string   "price_currency"
    t.string   "size"
    t.string   "upc"
    t.datetime "updated_at",             null: false
    t.string   "variation_id"
    t.text     "variation_secondaryids"
    t.float    "weight"
    t.float    "width"
    t.string   "sem3_help"
    t.string   "format"
    t.string   "author"
    t.string   "publisher"
  end

  create_table "progress_bars", force: :cascade do |t|
    t.string   "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float    "percent"
  end

  create_table "searchterms", force: :cascade do |t|
    t.string   "query"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
