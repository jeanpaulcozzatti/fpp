class WarmCacheJob < ActiveJob::Base


  def perform(terms,prog)



    prog.update_attributes!({
                              message: 'Warming the cache ...',
                              percent: 0
    })

    total_terms = terms.length
    ap total_terms
    i = 0.0
    ratio = 0.0
    terms.each do |term|
      ProductsHelper.GetProducts(term)
      sleep(1)

      i+=1.0
      ratio = (i / total_terms)*100
      puts ratio
      prog.update_attributes!({percent: ratio})

    end

    prog.update_attributes!({
                              message: "Finished! Let's search for Nexus",
                              percent: 100
    })
  end


end
