json.array!(@products) do |product|
  json.extract! product, :id, :sem3_id, :name, :sitedetails, :brand, :cat_id, :category, :color, :created_at, :description, :ean, :features, :gtins, :geo, :height, :images, :images_total, :length, :manufacturer, :model, :mpn, :price, :price_currency, :size, :upc, :updated_at, :variation_id, :variation_secondaryids, :weight, :width
  json.url product_url(product, format: :json)
end
