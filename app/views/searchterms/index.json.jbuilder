json.array!(@searchterms) do |searchterm|
  json.extract! searchterm, :id, :query
  json.url searchterm_url(searchterm, format: :json)
end
