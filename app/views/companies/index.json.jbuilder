json.array!(@companies) do |company|
  json.extract! company, :id, :name, :title, :description
  json.url company_url(company, format: :json)
end
