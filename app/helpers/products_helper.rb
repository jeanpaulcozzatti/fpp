module ProductsHelper

  require 'semantics3'

  # Your Semantics3 API Credentials

  API_KEY = 'SEM31A43B565A507E41AA6200E14BCBEC051'
  API_SECRET = 'MjU3NDgxMjE0MjI1YzMwYzcyNTVhM2VkZjFmMzM3YzY'

  def GetProducts(searchterm)
    #make this a helpe
    searchterm = "#{searchterm}"
    ap searchterm

    @products = Rails.cache.fetch(searchterm, expires_in: 12.hours) do
      # Set up a client to talk to the Semantics3 API
      sem3 = Semantics3::Products.new(API_KEY,API_SECRET)
      #          # Build the request
      sem3.products_field( "fields",["sitedetails", "name", "price", "brand"])
      sem3.products_field( "activeproductsonly", 1)
      sem3.products_field( "search", searchterm)

      #      # Run the request
      productsHash = sem3.get_products()
      ap productsHash

      #      # View the results of the reques
      #      # Run the request
      productsHash = sem3.iterate_products
      #      # View the results of the request
      # todo
      #this is wonky and could use some sprucing.
      @products_json = productsHash.to_json
      ap @products_json
      @products = [] if @products_json == "null"
      unless @products_json == "null"
        @productsy = JSON.parse(@products_json)
        @products = @productsy["results"]
        Rails.cache.write(searchterm,@products)  
      end
      
      @products
      #Product.create(@products)
    end
  end
end
