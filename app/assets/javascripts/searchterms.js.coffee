# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# app/javascripts/progress_bar.coffee

class @ProgressBar
  constructor: (elem, url) ->
    @elem = $(elem)
    @url = url

    @message = @elem.find('.message')
    @bar = @elem.find('.progress-bar')
    @pingTime = parseInt(@elem.data('ping-time'))

  start: => 
    $.ajax({
      url: @url,
      dataType: 'json',
      success: (data) =>	
        @message.html(data.message)

        percent = "#{data.percent}%"
        @bar.css('width', percent).html(percent)

        if data.percent < 100
          setTimeout(@start, @pingTime)
        else
          window.location = "/products/search/nexus"
    })