require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  setup do
    @product = products(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:products)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product" do
    assert_difference('Product.count') do
      post :create, product: { brand: @product.brand, cat_id: @product.cat_id, category: @product.category, color: @product.color, created_at: @product.created_at, description: @product.description, ean: @product.ean, features: @product.features, geo: @product.geo, gtins: @product.gtins, height: @product.height, images: @product.images, images_total: @product.images_total, length: @product.length, manufacturer: @product.manufacturer, model: @product.model, mpn: @product.mpn, name: @product.name, price: @product.price, price_currency: @product.price_currency, sem3_id: @product.sem3_id, sitedetails: @product.sitedetails, size: @product.size, upc: @product.upc, updated_at: @product.updated_at, variation_id: @product.variation_id, variation_secondaryids: @product.variation_secondaryids, weight: @product.weight, width: @product.width }
    end

    assert_redirected_to product_path(assigns(:product))
  end

  test "should show product" do
    get :show, id: @product
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product
    assert_response :success
  end

  test "should update product" do
    patch :update, id: @product, product: { brand: @product.brand, cat_id: @product.cat_id, category: @product.category, color: @product.color, created_at: @product.created_at, description: @product.description, ean: @product.ean, features: @product.features, geo: @product.geo, gtins: @product.gtins, height: @product.height, images: @product.images, images_total: @product.images_total, length: @product.length, manufacturer: @product.manufacturer, model: @product.model, mpn: @product.mpn, name: @product.name, price: @product.price, price_currency: @product.price_currency, sem3_id: @product.sem3_id, sitedetails: @product.sitedetails, size: @product.size, upc: @product.upc, updated_at: @product.updated_at, variation_id: @product.variation_id, variation_secondaryids: @product.variation_secondaryids, weight: @product.weight, width: @product.width }
    assert_redirected_to product_path(assigns(:product))
  end

  test "should destroy product" do
    assert_difference('Product.count', -1) do
      delete :destroy, id: @product
    end

    assert_redirected_to products_path
  end
end
