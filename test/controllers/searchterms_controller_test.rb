require 'test_helper'

class SearchtermsControllerTest < ActionController::TestCase
  setup do
    @searchterm = searchterms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:searchterms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create searchterm" do
    assert_difference('Searchterm.count') do
      post :create, searchterm: { query: @searchterm.query }
    end

    assert_redirected_to searchterm_path(assigns(:searchterm))
  end

  test "should show searchterm" do
    get :show, id: @searchterm
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @searchterm
    assert_response :success
  end

  test "should update searchterm" do
    patch :update, id: @searchterm, searchterm: { query: @searchterm.query }
    assert_redirected_to searchterm_path(assigns(:searchterm))
  end

  test "should destroy searchterm" do
    assert_difference('Searchterm.count', -1) do
      delete :destroy, id: @searchterm
    end

    assert_redirected_to searchterms_path
  end
end
