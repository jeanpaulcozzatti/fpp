Sidekiq.configure_server do |config|
  config.redis = { size: 27, url: ENV["REDISTOGO_URL"], namespace: "fpp" }
end

Sidekiq.configure_client do |config|
  config.redis = { size: 27, url: ENV["REDISTOGO_URL"], namespace: "fpp" }

end